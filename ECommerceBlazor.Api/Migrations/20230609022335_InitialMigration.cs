﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ECommerceBlazor.Api.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CartItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CartId = table.Column<int>(type: "int", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CartItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Carts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    Create = table.Column<bool>(type: "bit", nullable: false),
                    Read = table.Column<bool>(type: "bit", nullable: false),
                    Update = table.Column<bool>(type: "bit", nullable: false),
                    Delete = table.Column<bool>(type: "bit", nullable: false),
                    TableName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: false),
                    DeletedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: false),
                    DeletedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageURL = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    Verified = table.Column<bool>(type: "bit", nullable: false),
                    VerifiedBy = table.Column<int>(type: "int", nullable: true),
                    LastLogin = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Carts",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7521), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7530), 5 },
                    { 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7557), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7566), 7 }
                });

            migrationBuilder.InsertData(
                table: "ProductCategories",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "Name", "UpdatedAt", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7601), 2, null, null, "Beauty", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7616), 2 },
                    { 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7650), 3, null, null, "Furniture", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7660), 3 },
                    { 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7684), 2, null, null, "Electronics", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7694), 2 },
                    { 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7717), 4, null, null, "Shoes", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7727), 4 }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "CreatedAt", "DeletedAt", "Description", "ImageURL", "Name", "Price", "Quantity", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6227), null, "A kit provided by Glossier, containing skin care, hair care and makeup products", "/Images/Beauty/Beauty1.png", "Glossier - Beauty Kit", 100m, 100, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6237) },
                    { 2, 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6269), null, "A kit provided by Curology, containing skin care products", "/Images/Beauty/Beauty2.png", "Curology - Skin Care Kit", 50m, 45, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6278) },
                    { 3, 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6420), null, "A kit provided by Curology, containing skin care products", "/Images/Beauty/Beauty3.png", "Cocooil - Organic Coconut Oil", 20m, 30, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6431) },
                    { 4, 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6457), null, "A kit provided by Schwarzkopf, containing skin care and hair care products", "/Images/Beauty/Beauty4.png", "Schwarzkopf - Hair Care and Skin Care Kit", 50m, 60, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6466) },
                    { 5, 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6487), null, "Skin Care Kit, containing skin care and hair care products", "/Images/Beauty/Beauty5.png", "Skin Care Kit", 30m, 85, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6497) },
                    { 6, 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6526), null, "Air Pods - in-ear wireless headphones", "/Images/Electronic/Electronics1.png", "Air Pods", 100m, 120, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6536) },
                    { 7, 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6558), null, "On-ear Golden Headphones - these headphones are not wireless", "/Images/Electronic/Electronics2.png", "On-ear Golden Headphones", 40m, 200, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6568) },
                    { 8, 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6589), null, "On-ear Black Headphones - these headphones are not wireless", "/Images/Electronic/Electronics3.png", "On-ear Black Headphones", 40m, 300, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6598) },
                    { 9, 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6620), null, "Sennheiser Digital Camera - High quality digital camera provided by Sennheiser - includes tripod", "/Images/Electronic/Electronic4.png", "Sennheiser Digital Camera with Tripod", 600m, 20, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6630) },
                    { 10, 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6655), null, "Canon Digital Camera - High quality digital camera provided by Canon", "/Images/Electronic/Electronic5.png", "Canon Digital Camera", 500m, 15, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6665) },
                    { 11, 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6686), null, "Gameboy - Provided by Nintendo", "/Images/Electronic/technology6.png", "Nintendo Gameboy", 100m, 60, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6696) },
                    { 12, 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6718), null, "Very comfortable black leather office chair", "/Images/Furniture/Furniture1.png", "Black Leather Office Chair", 50m, 212, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6728) },
                    { 13, 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6751), null, "Very comfortable pink leather office chair", "/Images/Furniture/Furniture2.png", "Pink Leather Office Chair", 50m, 112, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6760) },
                    { 14, 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6781), null, "Very comfortable lounge chair", "/Images/Furniture/Furniture3.png", "Lounge Chair", 70m, 90, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6790) },
                    { 15, 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6812), null, "Very comfortable Silver lounge chair", "/Images/Furniture/Furniture4.png", "Silver Lounge Chair", 120m, 95, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6821) },
                    { 16, 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6843), null, "White and blue Porcelain Table Lamp", "/Images/Furniture/Furniture6.png", "Porcelain Table Lamp", 15m, 100, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6852) },
                    { 17, 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6874), null, "Office Table Lamp", "/Images/Furniture/Furniture7.png", "Office Table Lamp", 20m, 73, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6883) },
                    { 18, 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6908), null, "Comfortable Puma Sneakers in most sizes", "/Images/Shoes/Shoes1.png", "Puma Sneakers", 100m, 50, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6918) },
                    { 19, 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6940), null, "Colorful trainsers - available in most sizes", "/Images/Shoes/Shoes2.png", "Colorful Trainers", 150m, 60, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6949) },
                    { 20, 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6970), null, "Blue Nike Trainers - available in most sizes", "/Images/Shoes/Shoes3.png", "Blue Nike Trainers", 200m, 70, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6979) },
                    { 21, 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7002), null, "Colorful Hummel Trainers - available in most sizes", "/Images/Shoes/Shoes4.png", "Colorful Hummel Trainers", 120m, 120, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7012) },
                    { 22, 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7034), null, "Red Nike Trainers - available in most sizes", "/Images/Shoes/Shoes5.png", "Red Nike Trainers", 200m, 100, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7043) },
                    { 23, 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7066), null, "Birkenstock Sandles - available in most sizes", "/Images/Shoes/Shoes6.png", "Birkenstock Sandles", 50m, 150, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7075) }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "Name", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6061), null, "super", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6088) },
                    { 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6155), null, "admin", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6164) },
                    { 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6185), null, "user", new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(6195) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "LastLogin", "Password", "RoleId", "UpdatedAt", "Username", "Verified", "VerifiedBy" },
                values: new object[,]
                {
                    { 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7106), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7124), "$2a$11$.iMu7iIO3rDq8Gq5uIjGUeeaJ0atwE7w3JaHsjMCPtITOIhY9swnK", 1, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7115), "Eltama", true, 1 },
                    { 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7155), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7173), "$2a$11$LN3KAqI/yB8Zd38K5ewQU.Us6YTKieETpkTDMvaEwjrgCyLzSBrE6", 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7164), "Kanya", true, 1 },
                    { 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7195), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7212), "$2a$11$LN3KAqI/yB8Zd38K5ewQU.Us6YTKieETpkTDMvaEwjrgCyLzSBrE6", 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7203), "Rebeca", true, 1 },
                    { 4, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7233), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7251), "$2a$11$LN3KAqI/yB8Zd38K5ewQU.Us6YTKieETpkTDMvaEwjrgCyLzSBrE6", 2, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7242), "Fijaya Tirta", true, 1 },
                    { 5, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7378), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7397), "$2a$11$LN3KAqI/yB8Zd38K5ewQU.Us6YTKieETpkTDMvaEwjrgCyLzSBrE6", 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7388), "Gea Chandradewi", true, 4 },
                    { 6, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7427), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7446), "$2a$11$LN3KAqI/yB8Zd38K5ewQU.Us6YTKieETpkTDMvaEwjrgCyLzSBrE6", 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7436), "Santia Oktaveera", true, 3 },
                    { 7, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7469), null, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7488), "$2a$11$LN3KAqI/yB8Zd38K5ewQU.Us6YTKieETpkTDMvaEwjrgCyLzSBrE6", 3, new DateTime(2023, 6, 9, 9, 23, 34, 319, DateTimeKind.Local).AddTicks(7478), "Sanjay Darmawan", false, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CartItems");

            migrationBuilder.DropTable(
                name: "Carts");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "ProductCategories");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
