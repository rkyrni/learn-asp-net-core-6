﻿using ECommerce.Models.Dtos;

namespace ECommerceBlazor.Api.Service.Contracts
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> GetItems();
        Task<ProductDetailDto> GetItem(int id);
    }
}
