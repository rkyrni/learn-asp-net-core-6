﻿using ECommerce.Models.Dtos;
using ECommerceBlazor.Api.Extensions;
using ECommerceBlazor.Api.Repositories.Contracts;
using ECommerceBlazor.Api.Service.Contracts;
using ECommerceBlazor.Models.Response;

namespace ECommerceBlazor.Api.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;

        public ProductService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<IEnumerable<ProductDto>> GetItems()
        {
            var products = await this.productRepository.GetItems();

            return (from item in products
                    select new ProductDto
                    {
                        Id = item.Id,
                        Name = item.Name,
                        ImageURL = item.ImageURL,
                        Price = item.Price
                    }
                );
        }

        public async Task<ProductDetailDto> GetItem(int id)
        {
            var product = await this.productRepository.GetItem(id);

            var productCategory =  await this.productRepository.GetCategory(product.CategoryId);

            return product.ConvertToDto(productCategory);
        }
    }
}
