﻿using ECommerce.Models.Dtos;
using ECommerceBlazor.Models.Response;
using Microsoft.AspNetCore.Mvc;
using ECommerceBlazor.Api.Service.Contracts;

namespace ECommerceBlazor.Api.Controllers
{
    [Route("api/v1/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        public async Task<ActionResult<Response<IEnumerable<ProductDto>>>> GetItems()
        {
            try
            {
                var productDtos = await this.productService.GetItems();

                if (!productDtos.Any()) return NoContent();

                return Ok(new Response<IEnumerable<ProductDto>>(productDtos));
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<Response<ProductDetailDto>>> GetItem([FromRoute] int id)
        {
            try
            {
                var product = await this.productService.GetItem(id);
                if (product == null) return NotFound();

                return Ok(new Response<ProductDetailDto>(product));
            }
            catch
            {
                return BadRequest(new Response<object>());
            }
        }
    }
}
