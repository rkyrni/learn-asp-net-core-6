﻿using ECommerce.Models.Dtos;
using ECommerceBlazor.Api.Entities;

namespace ECommerceBlazor.Api.Extensions
{
    public static class DtoConversions
    {
        public static ProductDetailDto ConvertToDto(this Product product, ProductCategory productCategory)
        {
            var dt = new ProductDetailDto
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                ImageURL = product.ImageURL,
                Price = product.Price,
                Quantity = product.Quantity,
                CategoryId = productCategory.Id,
                CategoryName = productCategory.Name,
                CreatedAt = product.CreatedAt,
                UpdatedAt = product.UpdatedAt
            };

            return dt;

        }
    }
}
