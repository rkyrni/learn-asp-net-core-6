﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace ECommerceBlazor.Api.Entities
{
    public class ProductCategory
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        [AllowNull]
        public DateTime? DeletedAt { get; set; }

        public int CreatedBy { get; set; }

        [Required]
        public int UpdatedBy { get; set; }

        [AllowNull]
        public int? DeletedBy { get; set; }
    }
}
