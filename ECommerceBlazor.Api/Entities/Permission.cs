﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace ECommerceBlazor.Api.Entities
{
    public class Permission
    {
        [Key]
        public int Id { get; set; }
        public int RoleId { get; set; }
        public bool Create { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public string TableName { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        [AllowNull]
        public DateTime? DeletedAt { get; set; }

        public int CreatedBy { get; set; }

        [Required]
        public int UpdatedBy { get; set; }

        [AllowNull]
        public int? DeletedBy { get; set; }
    }
}
