﻿using ECommerceBlazor.Api.Data;
using ECommerceBlazor.Api.Entities;
using ECommerceBlazor.Api.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace ECommerceBlazor.Api.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ECommerceDbContext dbContext;
        public ProductRepository(ECommerceDbContext eCommerceDbContext)
        {
            this.dbContext = eCommerceDbContext;
        }
        public async Task<IEnumerable<ProductCategory>> GetCategories()
        {
            var categories = await dbContext.ProductCategories.ToListAsync();
            return categories;
        }

        public async Task<ProductCategory> GetCategory(int id)
        {
            var category = await dbContext.ProductCategories.FindAsync(id);

            return category;
        }

        public async Task<Product> GetItem(int id)
        {
            var product = await dbContext.Products.FindAsync(id);

            return product;
        }

        public async Task<IEnumerable<Product>> GetItems()
        {
            var products = await dbContext.Products.ToListAsync();

            return products;
        }
    }
}
